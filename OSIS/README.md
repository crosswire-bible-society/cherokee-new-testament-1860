# OSIS files

This directory contains three OSIS files, the second one of which was conveniently inserted by my TextPipe filter
- Che1860.osis
- Che1860.revisionDesc.osis
- Che1860.mend.osis

The first was output from the initial build of module **Che1860** using the *deprecated* tool `mod2osis`.

The last was made from the first by a *bespoke* TextPipe filter designed to
1. Repair the *faulty* OSIS XML output by `mod2osis`
2. Insert the missing **book titles** that were lost by `mod2osis`
3. Insert the **revisionDesc** elements into the **header**

The last OSIS file was used for the second build of module **Che1860** made using `osis2mod`.

That alone is the one that should be used for the official module update submission.
