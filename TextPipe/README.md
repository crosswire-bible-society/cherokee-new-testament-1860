# TextPipe filters and external replace lists

This directory will eventually contain my *bespoke* **TextPipe** filters developed in this project.

TextPipe .fll files are now saved as "easily readable" UTF-8 text files in JSON file format.

NB. Paths for input/output files and external replace lists are different on my PC than how this project is structured here.

## Status
*Files yet to be uploaded*.
