# Cherokee NT Source

This directory contains the source files in IMP format as extracted from the HTML files in the downloaded GitHub repository.
- Che1860.extract.imp.txt		contains the four line block for each verse, the first line being an image file link
- Che1860.extract.CS.imp.txt	contains the Cherokee Syllabics
- Che1860.extract.ST.imp.txt	contains the Sequoyah Transliteration
- Che1860.extract.YL.imp.txt	contains the Young's Literal English Transliteration

These were the starting points for all subsequent processing towards the final OSIS file.
