# Word Per Line files
This directory contains the IMP format text files after converting them to **WPL** layout.

It also contains the **Excel™ workbook** used to align and finely adjust the columns to facilitate applying **Sentence Case** to the **Sequoyah Transliterated forms** made using my _bespoke_ TextPipe filter with the **Cherokee Syllabics** as the input file.
