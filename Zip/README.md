# Archived SWORD module

This directory contains the SWORD module archived using the **Xiphos** module manager.
- Che1860.zip contains the final build made with **mod2osis**
- An intermediate build was made using **imp2vs** during module and text development.

The module is assigned as **Version 2.0** because it's a major update.

Only this final build from OSIS is intended for submission to CrossWire for release as an updated module.
- _It is provided here for testing purposes._
